﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkId=232509

(function () {
	"use strict";

	var app = WinJS.Application;
	var activation = Windows.ApplicationModel.Activation;
	var isFirstActivation = true;
	var pageList = [];
	var pageNumber = 0;
	var globalFilesInFolder;

	var angularApp = angular.module('myApp', []).directive('imageonload', function () {
	    return {
	        restrict: 'A',
	        link: function (scope, element, attrs) {
	            element.bind('load', function () {
	                //call the function that was passed
	                scope.$apply(attrs.imageonload);
	            });
	        }
	    };
	});
	angularApp.controller('mainController', function ($scope, $sce) {

	app.onactivated = function (args) {
		if (args.detail.kind === activation.ActivationKind.voiceCommand) {
			// TODO: Handle relevant ActivationKinds. For example, if your app can be started by voice commands,
			// this is a good place to decide whether to populate an input field or choose a different initial view.
		}
		else if (args.detail.kind === activation.ActivationKind.launch) {
			// A Launch activation happens when the user launches your app via the tile
			// or invokes a toast notification by clicking or tapping on the body.
			if (args.detail.arguments) {
				// TODO: If the app supports toasts, use this value from the toast payload to determine where in the app
				// to take the user in response to them invoking a toast notification.
			}
			else if (args.detail.previousExecutionState === activation.ApplicationExecutionState.terminated) {
				// TODO: This application had been suspended and was then terminated to reclaim memory.
				// To create a smooth user experience, restore application state here so that it looks like the app never stopped running.
				// Note: You may want to record the time when the app was last suspended and only restore state if they've returned after a short period.
			}
		}

		if (!args.detail.prelaunchActivated) {
			// TODO: If prelaunchActivated were true, it would mean the app was prelaunched in the background as an optimization.
			// In that case it would be suspended shortly thereafter.
			// Any long-running operations (like expensive network or disk I/O) or changes to user state which occur at launch
			// should be done here (to avoid doing them in the prelaunch case).
			// Alternatively, this work can be done in a resume or visibilitychanged handler.
		}

		if (isFirstActivation) {
		    
		        var applicationData = Windows.Storage.ApplicationData.current;
		        var localFolder = applicationData.localFolder;

		        localFolder.getFilesAsync().done(function (filesInFolder) {
		            globalFilesInFolder = filesInFolder;

		            var itemsProcessed = 0;
		            filesInFolder.forEach(function (file, index, array) {

		                if (file.fileType == ".html") {
		                    Windows.Storage.FileIO.readTextAsync(file).done(function (content) {

		                        pageList.push({ text: $sce.trustAsHtml(content), fileName: file.name });

		                        itemsProcessed++;
		                        if (itemsProcessed == array.length) {
		                            addImages(function () {
		                                //modify_page(pageList[pageNumber]);
		                                //$scope.page = pageList[pageNumber];
                                        $scope.pages = pageList;
                                        $scope.pages[pageNumber].visible = true;
		                                console.log($scope.page);
		                                $scope.$apply();
		                                setHeight();
		                            });
		                        }
		                    });
		                } else {
		                    itemsProcessed++;
		                    if (itemsProcessed == array.length) {
		                        addImages(function () {
		                            //modify_page(pageList[pageNumber]);
		                            //$scope.page = pageList[pageNumber];
		                            $scope.pages = pageList;
		                            $scope.pages[pageNumber].visible = true;
		                            console.log($scope.page);
		                            $scope.$apply();
		                            setHeight();
		                        });
		                    }
		                }
		            });
		        });

		        // TODO: The app was activated and had not been running. Do general startup initialization here.
		        document.getElementById("decrease-button").addEventListener("click", changePage, true);
		        document.getElementById("increase-button").addEventListener("click", changePage, true);
			    document.addEventListener("visibilitychange", onVisibilityChanged);
			    args.setPromise(WinJS.UI.processAll());
		}
		isFirstActivation = false;
	};



	function onVisibilityChanged(args) {
		if (!document.hidden) {
		    // TODO: The app just became visible. This may be a good time to refresh the view.
		    wheelzoom(document.querySelectorAll('img')); 
		    setHeight();

		    $(window).resize(function () {
		        setHeight();
		    });
		}
	}

	app.oncheckpoint = function (args) {
		// TODO: This application is about to be suspended. Save any state that needs to persist across suspensions here.
		// You might use the WinJS.Application.sessionState object, which is automatically saved and restored across suspension.
		// If you need to complete an asynchronous operation before your application is suspended, call args.setPromise().
	};

	app.start();

	function addImages(callback) {
	    var itemsProcessed = 0;
	    pageList.forEach(function (page, index, array) {
	        globalFilesInFolder.forEach(function (file) {
	            if (page.fileName != file.name) {
	                if (page.fileName.split(".")[0] == file.name.split(".")[0]) {
	                    itemsProcessed++;
	                    array[index].picture = file.path;
	                    var img = new Image();
	                    img.src = file.path;

	                    if (itemsProcessed == array.length) {
	                        return callback();
	                    }
	                }
	            }
	        });
	    });
	}

	function setHeight() {
	    var windowHeight = $(window).innerHeight();
	    var imageHeight = $('#main-image').innerHeight();
	    var imageWidth = $('#main-image').innerWidth();
	    var holderWidth = $('#image-holder').innerWidth();

	    if (imageHeight < windowHeight && imageHeight != 0) {
	        $('.img-responsive').css('margin-top', (windowHeight - imageHeight) / 2);
	    } else {
	        $('.img-responsive').css('margin-top', 0);
	    }
	    if (imageWidth < holderWidth) {
	        $('.img-responsive').css('margin-left', (holderWidth - imageWidth) / 2);
	    } else {
	        $('.img-responsive').css('margin-left', 0);
	    }

	};

	function changePage(element) {
	    $scope.loading = true;
	    $scope.pages[pageNumber].visible = false;
	    var pageNumberChange;
	    if (element.currentTarget.id == "decrease-button") {
	        pageNumberChange = -1;
	    } else {
	        pageNumberChange = 1;
	    }
	    pageNumber = pageNumber + pageNumberChange;
	    if (pageNumber == pageList.length) {
	        pageNumber = 0;
	    }
	    if (pageNumber < 0) {
	        pageNumber = pageList.length - 1;
	    }

	    $scope.pages[pageNumber].visible = true;
	    $scope.$apply();
	    wheelzoom(document.querySelectorAll('img'));
	}

	$scope.imageLoaded = function () {
	    setHeight();
	}
	});
})();